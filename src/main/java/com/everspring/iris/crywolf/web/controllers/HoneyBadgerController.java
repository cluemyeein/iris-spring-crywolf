package com.everspring.iris.crywolf.web.controllers;

import com.everspring.iris.crywolf.honeybadger.CustomNoticeReporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/honeybadger")
public class HoneyBadgerController {

    @Autowired
    private CustomNoticeReporter customNoticeReporter;

    @GetMapping("/")
    public String index() {
        return "Hello!  I'm a honey badger.";
    }

    @GetMapping("/throwerror")
    public String error2() {
        throw new IllegalStateException("Spring error");
    }

    @GetMapping("/report")
    public String report() {
        customNoticeReporter.notify(new Exception("See this error in HoneyBadger"));
        return "Notified HoneyBadger";
    }
}

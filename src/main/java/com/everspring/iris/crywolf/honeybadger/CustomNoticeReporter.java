package com.everspring.iris.crywolf.honeybadger;

import io.honeybadger.reporter.HoneybadgerReporter;
import io.honeybadger.reporter.NoticeReporter;
import io.honeybadger.reporter.config.StandardConfigContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CustomNoticeReporter {

    @Value("${honeybadger.api_key}")
    private String honeyBadgerApiKey;

    private NoticeReporter noticeReporter;

    public void notify(Throwable throwable) {
        noticeReporter = new HoneybadgerReporter(getConfig());
        noticeReporter.reportError(throwable);
    }

    private StandardConfigContext getConfig() {
        StandardConfigContext config = new StandardConfigContext();
        config.setApiKey(honeyBadgerApiKey)
                .setEnvironment("development")
                .setApplicationPackage("com.everspring.iris.scanner.honeybadger");
        return config;
    }
}
